<?php include('header.php'); ?>

<?php include('bib-parts/slider.php'); ?>
<?php include('bib-parts/chooseus.php'); ?>
<?php include('bib-parts/destination.php'); ?>
<?php include('bib-parts/destinations.php'); ?>
<?php include('bib-parts/offers.php'); ?>
<?php include('bib-parts/recenttours.php'); ?>
<?php include('bib-parts/testimonial.php'); ?>
<?php include('bib-parts/herosection.php'); ?>

<?php include('footer.php'); ?>