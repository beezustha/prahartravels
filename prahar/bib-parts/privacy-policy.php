<section class="privacy-policy ptb-100">
     <div class="container">
          <div class="row">
               <div class="col-lg-8 col-md-12">
                    <div class="content">
                         <div class="image">
                              <img src="assets/img/terms.jpg" alt="image">
                         </div>
                         <p>
                              <i>This Privacy and Policy was last updated on October, 2021.</i>
                         </p>
                         <h3>Welcome to Jaunt Privacy and Policy</h3>
                         <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has
                              been the industry's standard dummy text ever since the 1500s, when an unknown printer took
                              a galley of type and scrambled it to make a type specimen book. It has survived not only
                              five centuries, but also the leap into electronic typesetting, remaining essentially
                              unchanged.</p>
                         <h3>What are the benefits to work with Jaunt</h3>
                         <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has
                              been the industry's standard dummy text ever since the 1500s, when an unknown printer took
                              a galley of type and scrambled it to make a type specimen book. It has survived not only
                              five centuries, but also the leap into electronic typesetting, remaining essentially
                              unchanged.</p>
                         <h3>Data You Provide to Us</h3>
                         <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has
                              been the industry's standard dummy text ever since the 1500s, when an unknown printer took
                              a galley of type and scrambled it to make a type specimen book. It has survived not only
                              five centuries, but also the leap into electronic typesetting, remaining essentially
                              unchanged.</p>
                         <h3>Our Policy Concerning Children</h3>
                         <p class="m-0">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem
                              Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown
                              printer took a galley of type and scrambled it to make a type specimen book. It has
                              survived not only five centuries, but also the leap into electronic typesetting, remaining
                              essentially unchanged.</p>
                    </div>
               </div>
               <div class="col-lg-4">
                    <aside class="widget-area">
                         <section class="widget">
                              <ul>
                                   <li><a href="about-us.html"><i class="bx bx-chevron-right"></i>About Us</a></li>
                                   <li><a href="contact.html"><i class="bx bx-chevron-right"></i>Contact Us</a></li>
                                   <li><a href="faq.html"><i class="bx bx-chevron-right"></i>FAQ</a></li>
                                   <li><a href="privacy-policy.html" class="active"><i
                                                  class="bx bx-chevron-right"></i>Privacy & Policy</a></li>
                                   <li><a href="terms-of-service.html"><i class="bx bx-chevron-right"></i>Terms of
                                             Services</a></li>
                              </ul>
                         </section>
                    </aside>
               </div>
          </div>
     </div>
</section>