<section class="features-section pt-100 pb-70">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-md-6">
                <div class="item-single mb-30">
                    <i class="bx bx-calendar"></i>
                    <h3><a href="#">Reservation</a></h3>
                    <p>As compared with earlier times where a reservation personnel has to update its inventory every
                        time.</p>
                    <div class="cta-btn">
                        <a href="#" class="btn-primary">Read More</a>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6">
                <div class="item-single mb-30">
                    <i class="bx bxs-plane-take-off"></i>
                    <h3><a href="#">Tour Pack</a></h3>
                    <p>As compared with earlier times where a reservation personnel has to update its inventory every
                        time.</p>
                    <div class="cta-btn">
                        <a href="#" class="btn-primary">Read More</a>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 m-auto">
                <div class="item-single mb-30">
                    <i class="bx bx-money"></i>
                    <h3><a href="#">Payment</a></h3>
                    <p>As compared with earlier times where a reservation personnel has to update its inventory every
                        time.</p>
                    <div class="cta-btn">
                        <a href="#" class="btn-primary">Read More</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>