<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from templates.hibootstrap.com/jaunt/default/destination-details.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 27 Sep 2023 05:15:55 GMT -->

<head>
     <meta charset="UTF-8" />
     <meta name="viewport" content="width=device-width, initial-scale=1.0" />
     <meta name="description" content="Author: HiBootstrap, Category: Tourism, Multipurpose, HTML, SASS, Bootstrap" />

     <title>Jaunt - Travel & Tour Booking HTML Template</title>

     <link rel="stylesheet" href="assets/css/bootstrap.min.css" />

     <link rel="stylesheet" href="assets/css/fontawesome.min.css" />

     <link rel="stylesheet" href="assets/css/boxicons.min.css">

     <link rel="stylesheet" href="assets/css/animate.min.css" />

     <link rel="stylesheet" href="assets/css/bootstrap-datepicker.min.css">

     <link rel="stylesheet" href="assets/css/nice-select.css">

     <link rel="stylesheet" href="assets/css/magnific-popup.min.css" />

     <link rel="stylesheet" href="assets/css/owl.carousel.min.css" />

     <link rel="stylesheet" href="assets/css/meanmenu.min.css" />

     <link rel="stylesheet" href="assets/css/style.css" />

     <link rel="stylesheet" href="assets/css/responsive.css" />

     <link rel="stylesheet" href="assets/css/theme-dark.css" />

     <link rel="icon" href="assets/img/favicon.png" type="image/png" />
</head>

<body>
     <div class="page-title-area ptb-100">
          <div class="container">
               <div class="page-title-content">
                    <h1>Our Destinations</h1>
                    <ul>
                         <li class="item"><a href="index.php">Home</a></li>
                         <li class="item"><a href="destination-details.php"><i
                                        class="bx bx-chevrons-right"></i>Destinations
                                   Details</a></li>
                    </ul>
               </div>
          </div>
          <div class="bg-image">
               <img src="assets/img/page-title-area/destination-details.jpg" alt="Demo Image">
          </div>
     </div>


     <section class="destinations-details-section pt-100 pb-70">
          <div class="container">
               <div class="section-title">
                    <h2>Annapurna Base Camp, Nepal</h2>
               </div>
               <div class="row">
                    <div class="col-lg-8 col-md-12">
                         <div class="destination-details-desc mb-30">
                              <div class="row align-items-center">
                                   <div class="col-md-6 col-sm-12">
                                        <div class="image mb-30">
                                             <img src="assets/img/destination13.jpg" alt="Demo Image" />
                                        </div>
                                   </div>
                                   <div class="col-md-6 col-sm-12">
                                        <div class="image mb-30">
                                             <img src="assets/img/destination14.jpg" alt="Demo Image" />
                                        </div>
                                   </div>
                              </div>
                              <div class="content mb-20">
                                   <h3>Annapurna Base Camp, Nepal.</h3>
                                   <p>
                                        I have personally participated in many of the programs mentioned on this site.
                                        One of
                                        the programs is Save Our I have personally participated in many of the programs
                                        mentioned on this site. One of Save Our I have personally in many of the
                                        programs
                                        mentioned on this site.I have personally in many of the programs mentioned on
                                        this site.
                                        One of the programs is Save.
                                   </p>
                                   <p>
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                                        incididunt ut labore dolore magna aliqua. Quis ipsum suspendisse ultrices
                                        gravida. Risus
                                        commodo
                                   </p>
                              </div>
                              <div class="row align-items-center">

                                   <div class="content">
                                        <p class="mb-30">
                                             I have personally participated in many of the programs mentioned on this
                                             site. One
                                             of the programs is Save Our I have personally participated in many of the
                                             programs
                                             mentioned on this site. One of Save Our I have personally in many of the
                                             programs
                                             mentioned on this site.
                                        </p>
                                   </div>
                              </div>
                              <p class="mb-20">
                                   Lorem ipsum dolor sit amet, participated consectetur adipiscing elit, sed do eiusmod
                                   tempor
                                   incididunt ut labore dolore magna aliqua. Quis ipsum suspendisse ultrices gravida.
                                   Risus
                                   commodo viverra maecenas accumsan lacus
                                   vel facilisis consectetur adipiscing.
                              </p>
                              <div class="info-content">
                                   <h3 class="sub-title">Some Information</h3>
                                   <div class="row">
                                        <div class="col-lg-6 col-md-6">
                                             <div class="content-list">
                                                  <i class="bx bx-map-alt"></i>
                                                  <h6><span>Country :</span> Pokhara, Nepal</h6>
                                             </div>
                                        </div>
                                        <div class="col-lg-6 col-md-6">
                                             <div class="content-list">
                                                  <i class="bx bx-book-reader"></i>
                                                  <h6><span>Language Spoken :</span> Nepali</h6>
                                             </div>
                                        </div>
                                        <div class="col-lg-6 col-md-6">
                                             <div class="content-list">
                                                  <i class="bx bx-notepad"></i>
                                                  <h6><span>Visa Requirments :</span> Yes</h6>
                                             </div>
                                        </div>
                                        <div class="col-lg-6 col-md-6">
                                             <div class="content-list">
                                                  <i class="bx bx-area"></i>
                                                  <h6><span>Area (km2) :</span> 1770.80 km2</h6>
                                             </div>
                                        </div>
                                        <div class="col-lg-6 col-md-6">
                                             <div class="content-list">
                                                  <i class="bx bx-user"></i>
                                                  <h6><span>Per Person :</span> $1200</h6>
                                             </div>
                                        </div>
                                        <div class="col-lg-6 col-md-6">
                                             <div class="content-list">
                                                  <i class="bx bx-group"></i>
                                                  <h6><span>Guide :</span> Local Guide Available</h6>
                                             </div>
                                        </div>
                                   </div>
                              </div>
                              <p class="mb-20">
                                   Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                                   incididunt ut
                                   labore dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. Risus commodo
                                   viverra
                                   maecenas accumsan lacus
                                   vel facilisis consectetur adipiscing.
                              </p>
                              <hr>
                         </div>
                    </div>
                    <div class="col-lg-4 col-md-12">
                         <aside class=" widget-area">
                              <form>
                                   <div class="form-row bg-primary text-white">
                                        <div class="form-group col-md-6">
                                             <i class="fa-sharp fa-solid fa-user"></i>
                                             <input type="email" class="form-control" id="inputEmail4"
                                                  placeholder="Your Name">
                                        </div>
                                   </div>
                                   <div class="form-row">
                                        <div class="form-group">
                                             <label for="inputAddress">Address</label>
                                             <input type="text" class="form-control" id="inputAddress"
                                                  placeholder="1234 Main St">
                                        </div>
                                   </div>
                                   <div class="form-group">
                                        <label for="inputAddress2">Address 2</label>
                                        <input type="text" class="form-control" id="inputAddress2"
                                             placeholder="Apartment, studio, or floor">
                                   </div>
                                   <div class="form-row">
                                        <div class="form-group col-md-6">
                                             <label for="inputCity">City</label>
                                             <input type="text" class="form-control" id="inputCity">
                                        </div>
                                        <div class="form-group col-md-4">
                                             <label for="inputState">State</label>
                                             <select id="inputState" class="form-control">
                                                  <option selected>Choose...</option>
                                                  <option>...</option>
                                             </select>
                                        </div>
                                        <div class="form-group col-md-2">
                                             <label for="inputZip">Zip</label>
                                             <input type="text" class="form-control" id="inputZip">
                                        </div>
                                   </div>
                                   <div class="form-group">
                                        <div class="form-check">
                                             <input class="form-check-input" type="checkbox" id="gridCheck">
                                             <label class="form-check-label" for="gridCheck">
                                                  Check me out
                                             </label>
                                        </div>
                                   </div>
                                   <div class="col col-4 mt-2 "><button type="button" class="btn btn-sm btn-danger">Book
                                             Now</button>
                                   </div>
                              </form>


                              <div class="widget widget-gallery mb-30">
                                   <h3 class="sub-title">Instagram Post</h3>
                                   <ul class="instagram-post">
                                        <li>
                                             <img src="assets/img/instagram1.jpg" alt="Demo Image">
                                             <i class="bx bxl-instagram"></i>
                                        </li>
                                        <li>
                                             <img src="assets/img/instagram2.jpg" alt="Demo Image">
                                             <i class="bx bxl-instagram"></i>
                                        </li>
                                        <li>
                                             <img src="assets/img/instagram3.jpg" alt="Demo Image">
                                             <i class="bx bxl-instagram"></i>
                                        </li>
                                        <li>
                                             <img src="assets/img/instagram4.jpg" alt="Demo Image">
                                             <i class="bx bxl-instagram"></i>
                                        </li>
                                        <li>
                                             <img src="assets/img/instagram5.jpg" alt="Demo Image">
                                             <i class="bx bxl-instagram"></i>
                                        </li>
                                        <li>
                                             <img src="assets/img/instagram6.jpg" alt="Demo Image">
                                             <i class="bx bxl-instagram"></i>
                                        </li>
                                   </ul>
                              </div>
                         </aside>
                    </div>
               </div>
          </div>
     </section>

     <script src="assets/js/jquery.min.js"></script>

     <script src="assets/js/bootstrap.bundle.min.js"></script>

     <script src="assets/js/bootstrap-datepicker.min.js"></script>

     <script src="assets/js/jquery.nice-select.min.js"></script>

     <script src="assets/js/jquery.magnific-popup.min.js"></script>

     <script src="assets/js/jquery.filterizr.min.js"></script>

     <script src="assets/js/owl.carousel.min.js"></script>

     <script src="assets/js/meanmenu.min.js"></script>

     <script src="assets/js/form-validator.min.js"></script>

     <script src="assets/js/contact-form-script.js"></script>

     <script src="assets/js/jquery.ajaxchimp.min.js"></script>

     <script src="assets/js/script.js"></script>
</body>

<!-- Mirrored from templates.hibootstrap.com/jaunt/default/destination-details.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 27 Sep 2023 05:15:58 GMT -->

</html>