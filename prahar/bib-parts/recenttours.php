<section id="tours" class="tours-section pt-100 pb-70 bg-light">
    <div class="container">
        <div class="section-title">
            <h2>Recent Tours</h2>
            <!-- <p>Travel has helped us to understand the meaning of life and it has helped us become better people. Each
                time we travel, we see the world with new eyes.</p> -->
        </div>
        <div class="row no-wrap">
            <div class="col-auto">
                <div class="item-single mb-30">
                    <div class="image">
                        <img src="assets/img/tour/annapurna.jpg" alt="Annapurna Image" />
                    </div>
                    <div class="content">
                        <span class="location"><i class="bx bx-map"></i>Pokhara, Nepal</span>
                        <h3>
                            <a href="tours.html">Annapurna Base Camp</a>
                        </h3>

                        <p>
                            A wonderful little cottage right on the seashore - perfect for exploring with the little
                            boat which is included in the price. Located opposite Nidri sleeping.
                        </p>
                        <hr>
                        <ul class="list">
                            <li><i class="bx bx-time"></i>3 Days</li>
                            <li><i class="bx bx-group"></i>160+</li>
                            <li>$1500</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col col-md-12">
                <div class="tours-slider owl-carousel mb-30">
                    <div class="slider-item">
                        <div class="image">
                            <img src="assets/img/tour/tour2.jpg" alt="Demo Image" />
                        </div>
                        <div class="content">
                            <span class="location"><i class="bx bx-map"></i>Chitwan, Nepal</span>
                            <div class="title">
                                <h3>
                                    <a href="tours.html">Chitwan National Park</a>
                                </h3>
                            </div>
                            <ul class="list">
                                <li><i class="bx bx-time"></i>7 Days</li>
                                <li><i class="bx bx-group"></i>60+</li>
                                <li>$2100</li>
                            </ul>
                        </div>
                    </div>
                    <div class="slider-item">
                        <div class="image">
                            <img src="assets/img/tour/tour3.jpg" alt="Gosainkunda Image" />
                        </div>
                        <div class="content">
                            <span class="location"><i class="bx bx-map"></i>Rasuwa, Nepal</span>
                            <div class="title">
                                <h3>
                                    <a href="tours.html">Gosainkunda</a>
                                </h3>
                            </div>
                            <ul class="list">
                                <li><i class="bx bx-time"></i>5 Days</li>
                                <li><i class="bx bx-group"></i>60+</li>
                                <li>$1500</li>
                            </ul>
                        </div>
                    </div>
                    <div class="slider-item">
                        <div class="image">
                            <img src="assets/img/tour/tour4.jpg" alt="Demo Image" />
                        </div>
                        <div class="content">
                            <span class="location"><i class="bx bx-map"></i>Lumbini, Nepal</span>

                            <div class="title">
                                <h3>
                                    <a href="tours.html">Lumbini</a>
                                </h3>
                            </div>
                            <ul class="list">
                                <li><i class="bx bx-time"></i>7 Days</li>
                                <li><i class="bx bx-group"></i>60+</li>
                                <li>$2300</li>
                            </ul>
                        </div>
                    </div>
                    <div class="slider-item">
                        <div class="image">
                            <img src="assets/img/tour/tour5.jpg" alt="Demo Image" />
                        </div>
                        <div class="content">
                            <span class="location"><i class="bx bx-map"></i>Mugu, Nepal</span>
                            <div class="title">
                                <h3>
                                    <a href="tours.html">Rara</a>
                                </h3>
                            </div>
                            <ul class="list">
                                <li><i class="bx bx-time"></i>3 Days</li>
                                <li><i class="bx bx-group"></i>60+</li>
                                <li>$1200</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>