<section id="offers" class="offers-section pt-100 pb-70 bg-light">
    <div class="container">
        <div class="section-title">
            <h2>Special Offers & Discount</h2>

        </div>
        <div class="row">
            <div class="col-lg-4 col-md-6">
                <div class="item-single mb-30">
                    <div class="offer-image">
                        <img src="assets/img/annapurna.jpg" alt="Annapurna Image" />
                    </div>
                    <div class="content">
                        <div class="review">
                            <i class="bx bxs-star"></i>
                            <i class="bx bxs-star"></i>
                            <i class="bx bxs-star"></i>
                            <i class="bx bxs-star"></i>
                            <i class="bx bxs-star"></i>
                            <span>39 Review</span>
                        </div>
                        <div class="title">
                            <h3>
                                <a href="tours.html">Annapurna Base Camp</a>
                            </h3>
                            <span>$2000</span>
                        </div>
                        <ul class="list">
                            <li><i class="bx bx-time"></i>7 Days</li>
                            <li><i class="bx bx-group"></i>60+</li>
                            <li>$1500</li>
                        </ul>
                    </div>
                    <div class="discount">
                        <span>Discount</span>
                        <span>30%</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6">
                <div class="item-single mb-30">
                    <div class="offer-image">
                        <img src="assets/img/Everest.jpg" alt="Everest Image" />
                    </div>
                    <div class="content">
                        <div class="review">
                            <i class="bx bxs-star"></i>
                            <i class="bx bxs-star"></i>
                            <i class="bx bxs-star"></i>
                            <i class="bx bxs-star"></i>
                            <i class="bx bxs-star"></i>
                            <span>19 Review</span>
                        </div>
                        <div class="title">
                            <h3>
                                <a href="tours.html">Everest Base Camp</a>
                            </h3>
                            <span>$1600</span>
                        </div>
                        <ul class="list">
                            <li><i class="bx bx-time"></i>5 Days</li>
                            <li><i class="bx bx-group"></i>130+</li>
                            <li>$1200</li>
                        </ul>
                    </div>
                    <div class="discount">
                        <span>Discount</span>
                        <span>29%</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 m-auto">
                <div class="item-single mb-30">
                    <div class="offer-image">
                        <img src="assets/img/lumbini.jpg" alt="Lumbini Image" />
                    </div>
                    <div class="content">
                        <div class="review">
                            <i class="bx bxs-star"></i>
                            <i class="bx bxs-star"></i>
                            <i class="bx bxs-star"></i>
                            <i class="bx bxs-star"></i>
                            <i class="bx bxs-star"></i>
                            <span>35 Review</span>
                        </div>
                        <div class="title">
                            <h3>
                                <a href="tours.html">Lumbini</a>
                            </h3>
                            <span>$2600</span>
                        </div>
                        <ul class="list">
                            <li><i class="bx bx-time"></i>3 Days</li>
                            <li><i class="bx bx-group"></i>91+</li>
                            <li>$2200</li>
                        </ul>
                    </div>
                    <div class="discount">
                        <span>Discount</span>
                        <span>16%</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>