<section class="faq-section ptb-100 bg-light">
     <div class="container">
          <div class="section-title">
               <h2>FAQ</h2>
               <p>Travel has helped us to understand the meaning of life and it has helped us become better people. Each
                    time we travel, we see the world with new eyes.</p>
          </div>
          <div class="panel-group" id="accordion">
               <div class="row">
                    <div class="col-lg-12">
                         <div class="panel">
                              <div class="panel-heading">
                                   <h4 class="panel-title">
                                        <a class="collapsed" data-bs-toggle="collapse" href="#collapse1">
                                             What are the benefits to work with Jaunt?
                                        </a>
                                   </h4>
                              </div>
                              <div id="collapse1" class="panel-collapse collapse show" data-bs-parent="#accordion">
                                   <div class="panel-body">
                                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                             Lorem Ipsum has been the industry's standard dummy text ever since the
                                             1500s, when an unknown printer took a galley of type and scrambled it to
                                             make a type specimen book. It has survived not only five centuries, but
                                             also the leap into electronic typesetting, remaining essentially unchanged.
                                        </p>
                                   </div>
                              </div>
                         </div>
                         <div class="panel">
                              <div class="panel-heading">
                                   <h4 class="panel-title">
                                        <a class="collapsed" data-bs-toggle="collapse" href="#collapse2">
                                             Where can I find out about new places?
                                        </a>
                                   </h4>
                              </div>
                              <div id="collapse2" class="panel-collapse collapse" data-bs-parent="#accordion">
                                   <div class="panel-body">
                                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                             Lorem Ipsum has been the industry's standard dummy text ever since the
                                             1500s, when an unknown printer took a galley of type and scrambled it to
                                             make a type specimen book. It has survived not only five centuries, but
                                             also the leap into electronic typesetting, remaining essentially unchanged.
                                        </p>
                                   </div>
                              </div>
                         </div>
                         <div class="panel">
                              <div class="panel-heading">
                                   <h4 class="panel-title">
                                        <a class="collapsed" data-bs-toggle="collapse" href="#collapse3">
                                             How to pay a single tours?
                                        </a>
                                   </h4>
                              </div>
                              <div id="collapse3" class="panel-collapse collapse" data-bs-parent="#accordion">
                                   <div class="panel-body">
                                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                             Lorem Ipsum has been the industry's standard dummy text ever since the
                                             1500s, when an unknown printer took a galley of type and scrambled it to
                                             make a type specimen book. It has survived not only five centuries, but
                                             also the leap into electronic typesetting, remaining essentially unchanged.
                                        </p>
                                   </div>
                              </div>
                         </div>
                         <div class="panel m-0">
                              <div class="panel-heading">
                                   <h4 class="panel-title">
                                        <a class="collapsed" data-bs-toggle="collapse" href="#collapse4">
                                             What will happen when I’ve sent my application?
                                        </a>
                                   </h4>
                              </div>
                              <div id="collapse4" class="panel-collapse collapse" data-bs-parent="#accordion">
                                   <div class="panel-body">
                                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                             Lorem Ipsum has been the industry's standard dummy text ever since the
                                             1500s, when an unknown printer took a galley of type and scrambled it to
                                             make a type specimen book. It has survived not only five centuries, but
                                             also the leap into electronic typesetting, remaining essentially unchanged.
                                        </p>
                                   </div>
                              </div>
                         </div>
                    </div>
               </div>
          </div>
     </div>
</section>