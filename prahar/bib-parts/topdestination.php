<section id="top-destination" class="top-destination-section pb-70 pt-70">
    <div class="container">
        <div class="section-title">
            <h2>Top Destinations</h2>
            <p>Travel has helped us to understand the meaning of life and it has helped us become better people. Each
                time we travel, we see the world with new eyes.</p>
        </div>
        <div class="row">
            <div class="col-lg-4 col-md-6">
                <div class="item-single mb-30">
                    <div class="image">
                        <img src="assets/img/chitwan.jpg" alt="Demo Image">
                    </div>
                    <div class="content">
                        <span class="location"><i class="bx bx-map"></i>Chiwan, Nepal</span>
                        <h3>
                            <a href="destination-details.html">Chitwan National Park</a>
                        </h3>
                        <div class="review">
                            <i class="bx bx-smile"></i>
                            <span>9</span>
                            <span>Superb</span>
                        </div>
                        <p>
                            Two short getaway breaks in the Greece together and one mini caravan holiday.
                        </p>
                        <hr>
                        <ul class="list">
                            <li><i class="bx bx-time"></i>7 Days</li>
                            <li><i class="bx bx-group"></i>65+</li>
                            <li>$2000</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6">
                <div class="item-single mb-30">
                    <div class="image">
                        <img src="assets/img/gosainkunda.jpg" alt="Gosainkunda Image">
                    </div>
                    <div class="content">
                        <span class="location"><i class="bx bx-map"></i>Rasuwa, Nepal</span>
                        <h3>
                            <a href="destination-details.html">Gosainkunda</a>
                        </h3>
                        <div class="review">
                            <i class="bx bx-smile"></i>
                            <span>7.5</span>
                            <span>Amazing</span>
                        </div>
                        <p>
                            A simple hunting lodging and later a small château with a moat occupied the site.
                        </p>
                        <hr>
                        <ul class="list">
                            <li><i class="bx bx-time"></i>3 Days</li>
                            <li><i class="bx bx-group"></i>160+</li>
                            <li>$1500</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 m-auto">
                <div class="item-single mb-30">
                    <div class="image">
                        <img src="assets/img/rara.jpg" alt="Rara Image">
                    </div>
                    <div class="content">
                        <span class="location"><i class="bx bx-map"></i>Mugu, Nepal</span>
                        <h3>
                            <a href="destination-details.html">Rara</a>
                        </h3>
                        <div class="review">
                            <i class="bx bx-smile"></i>
                            <span>8.5</span>
                            <span>Superb</span>
                        </div>
                        <p>
                            The gorgeous play of light did justice to the mystique of the ancient ruins that.
                        </p>
                        <hr>
                        <ul class="list">
                            <li><i class="bx bx-time"></i>3 Days</li>
                            <li><i class="bx bx-group"></i>160+</li>
                            <li>$1500</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>