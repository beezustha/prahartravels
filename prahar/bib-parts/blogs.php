<section id="blog" class="blog-section blog-style-two ptb-100 bg-light">
<div class="container">
<div class="section-title">
<h2>Latest News & Blog</h2>
<p>Travel has helped us to understand the meaning of life and it has helped us become better people. Each time we travel, we see the world with new eyes.</p>
</div>
<div class="row">
<div class="col-lg-4 col-md-6">
<div class="item-single mb-30">
<div class="image">
<img src="assets/img/blog/blog1.jpg" alt="Demo Image" />
</div>
<div class="content">
<ul class="info-list">
<li><i class="bx bx-calendar"></i> Oct 10, 2021</li>
</ul>
<h3>
<a href="blog-details.html">Hang on the beach with little sea turtles.</a>
</h3>
<p>
I have personally participated in many of the programs mentioned on this site. One of the programs is Save Our Sea...
</p>
</div>
</div>
</div>
<div class="col-lg-4 col-md-6">
<div class="item-single mb-30">
<div class="image">
<img src="assets/img/blog/blog2.jpg" alt="Demo Image" />
</div>
<div class="content">
<ul class="info-list">
<li><i class="bx bx-calendar"></i> Nov 10, 2021</li>
</ul>
<h3>
<a href="blog-details.html">In nightlife you can do anything you want.</a>
</h3>
<p>
I am a self-proclaimed experience junkie. There is nothing I love more than going somewhere or doing something new...
</p>
</div>
</div>
</div>
<div class="col-lg-4 col-md-6">
<div class="item-single mb-30">
<div class="image">
<img src="assets/img/blog/blog3.jpg" alt="Demo Image" />
</div>
<div class="content">
<ul class="info-list">
<li><i class="bx bx-calendar"></i> Oct 5, 2021</li>
</ul>
<h3>
<a href="blog-details.html">Travel survival tips: airports & flights.</a>
</h3>
<p>
I recently returned from a three-week trip to the Netherlands, United Kingdom, India, and Hong Kong. I was on 13 planes...
</p>
</div>
</div>
</div>
<div class="col-lg-4 col-md-6">
<div class="item-single mb-30">
<div class="image">
<img src="assets/img/blog/blog4.jpg" alt="Demo Image" />
</div>
<div class="content">
<ul class="info-list">
<li><i class="bx bx-calendar"></i> Oct 10, 2021</li>
</ul>
<h3>
<a href="blog-details.html">Travel is fatal to prejudice and bigotry.</a>
</h3>
<p>
I have personally participated in many of the programs mentioned on this site. One of the programs is Save Our Sea...
</p>
</div>
</div>
</div>
<div class="col-lg-4 col-md-6">
<div class="item-single mb-30">
<div class="image">
<img src="assets/img/blog/blog5.jpg" alt="Demo Image" />
</div>
<div class="content">
<ul class="info-list">
<li><i class="bx bx-calendar"></i> Nov 10, 2021</li>
</ul>
<h3>
<a href="blog-details.html">The real voyage does not consist in seeking.</a>
</h3>
<p>
I am a self-proclaimed experience junkie. There is nothing I love more than going somewhere or doing something new...
</p>
</div>
</div>
</div>
<div class="col-lg-4 col-md-6">
<div class="item-single mb-30">
<div class="image">
<img src="assets/img/blog/blog6.jpg" alt="Demo Image" />
</div>
<div class="content">
<ul class="info-list">
<li><i class="bx bx-calendar"></i> Oct 5, 2021</li>
</ul>
<h3>
<a href="blog-details.html">Fear is only temporary regrets last forever.</a>
</h3>
<p>
I recently returned from a three-week trip to the Netherlands, United Kingdom, India, and Hong Kong. I was on 13 planes...
</p>
</div>
</div>
</div>
<div class="col-lg-4 col-md-6">
<div class="item-single mb-30">
<div class="image">
<img src="assets/img/blog/blog7.jpg" alt="Demo Image" />
</div>
<div class="content">
<ul class="info-list">
<li><i class="bx bx-calendar"></i> Oct 10, 2021</li>
</ul>
<h3>
<a href="blog-details.html">Stop worrying about the potholes.</a>
</h3>
<p>
I have personally participated in many of the programs mentioned on this site. One of the programs is Save Our Sea...
</p>
</div>
</div>
</div>
<div class="col-lg-4 col-md-6">
<div class="item-single mb-30">
<div class="image">
<img src="assets/img/blog/blog8.jpg" alt="Demo Image" />
</div>
<div class="content">
<ul class="info-list">
<li><i class="bx bx-calendar"></i> Nov 10, 2021</li>
</ul>
<h3>
<a href="blog-details.html">Life begins at the end of your comfort zone.</a>
</h3>
<p>
I am a self-proclaimed experience junkie. There is nothing I love more than going somewhere or doing something new...
</p>
</div>
</div>
</div>
<div class="col-lg-4 col-md-6 m-auto">
<div class="item-single mb-30">
<div class="image">
<img src="assets/img/blog/blog9.jpg" alt="Demo Image" />
</div>
<div class="content">
<ul class="info-list">
<li><i class="bx bx-calendar"></i> Oct 5, 2021</li>
</ul>
<h3>
<a href="blog-details.html">Fear is only temporary regrets last forever.</a>
</h3>
<p>
I recently returned from a three-week trip to the Netherlands, United Kingdom, India, and Hong Kong. I was on 13 planes...
</p>
</div>
</div>
</div>
<div class="col-lg-12 col-md-12">
<div class="pagination text-center">
<span class="page-numbers current" aria-current="page">1</span>
<a href="#" class="page-numbers">2</a>
<a href="#" class="page-numbers">3</a>
<a href="#" class="page-numbers">Next</a>
</div>
</div>
</div>
</div>
</section>