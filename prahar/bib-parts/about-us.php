<section id="about" class="about-section about-style-three ptb-100">
     <div class="container">
          <div class="row align-items-center">
               <div class="col-lg-10 m-auto">
                    <div class="about-content">
                         <div class="row">
                              <div class="col-12">
                                   <h2>
                                        About Prahar Travels
                                   </h2>
                                   <h6>
                                        Top Tour Operators and Travel Agency. We offering in total 793 tours and
                                        holidays through-out the world. Combined we have received 1532 customer reviews.
                                   </h6>
                                   <p>
                                        Travel has helped us to understand the meaning of life and it has helped us
                                        become better people. Each time we travel, we see the world with new eyes.Travel
                                        has helped us to understand the meaning of life and it has helped us become
                                        better people. Each time we travel, we see the world with new eyes.Travel has
                                        helped us to understand the meaning of life and it has helped us.
                                   </p>
                              </div>
                         </div>
                         <div class="col-lg-10 m-auto">
                              <div class="row">
                                   <div class="col-lg-4 col-md-6">
                                        <div class="content-list">
                                             <i class="bx bx-check-shield"></i>
                                             <h6>Safety Travel System</h6>
                                        </div>
                                   </div>
                                   <div class="col-lg-4 col-md-6">
                                        <div class="content-list">
                                             <i class="bx bx-donate-heart"></i>
                                             <h6>Budget-Friendly Tour</h6>
                                        </div>
                                   </div>
                                   <div class="col-lg-4 col-md-6">
                                        <div class="content-list">
                                             <i class="bx bx-support"></i>
                                             <h6>24/7 Customer Support</h6>
                                        </div>
                                   </div>
                                   <div class="col-lg-4 col-md-6">
                                        <div class="content-list">
                                             <i class="bx bx-time"></i>
                                             <h6>Expert Trip Planning</h6>
                                        </div>
                                   </div>
                                   <div class="col-lg-4 col-md-6">
                                        <div class="content-list">
                                             <i class="bx bx-station"></i>
                                             <h6>Fast Communication</h6>
                                        </div>
                                   </div>
                                   <div class="col-lg-4 col-md-6">
                                        <div class="content-list">
                                             <i class="bx bx-like"></i>
                                             <h6>Right Solution & Guide</h6>
                                        </div>
                                   </div>
                              </div>
                         </div>
                         <div class="about-btn">
                              <a href="contact-us.php" class="btn-primary">Contact Us</a>
                              <a href="about-us.html" class="btn-primary">Read More</a>
                         </div>
                    </div>
               </div>
          </div>
     </div>
     <div class="shape shape-1"><img src="assets/img/shape1.png" alt="Background Shape"></div>
     <div class="shape shape-2"><img src="assets/img/shape2.png" alt="Background Shape"></div>
     <div class="shape shape-3"><img src="assets/img/shape3.png" alt="Background Shape"></div>
     <div class="shape shape-4"><img src="assets/img/shape4.png" alt="Background Shape"></div>
</section>


<section id="team" class="team-section ptb-100">
     <div class="container">
          <div class="section-title">
               <h2>Our Team & Guide</h2>
               <p>Travel has helped us to understand the meaning of life and it has helped us become better people. Each
                    time we travel, we see the world with new eyes.</p>
          </div>
          <div class="row">
               <div class=" col-lg-3 col-sm-6 col-md-6">
                    <div class="item-single mb-30">
                         <div class="image">
                              <img src="assets/img/team/team1.jpg" alt="Demo Image">
                         </div>
                         <div class="content">
                              <div class="title">
                                   <h3>
                                        <a href="team.html">David Stiffen</a>
                                   </h3>
                                   <span>Company Founder</span>
                              </div>
                              <div class="social-link">
                                   <a href="#" target="_blank"><i class="bx bxl-facebook"></i></a>
                                   <a href="#" target="_blank"><i class="bx bxl-twitter"></i></a>
                                   <a href="#" target="_blank"><i class="bx bxl-linkedin"></i></a>
                                   <a href="#" target="_blank"><i class="bx bxl-instagram"></i></a>
                              </div>
                         </div>
                    </div>
               </div>
               <div class="col-lg-3 col-sm-6 col-md-6">
                    <div class="item-single mb-30">
                         <div class="image">
                              <img src="assets/img/team/team2.jpg" alt="Demo Image">
                         </div>
                         <div class="content">
                              <div class="title">
                                   <h3>
                                        <a href="team.html">Thomas Alis</a>
                                   </h3>
                                   <span>Tour Planner</span>
                              </div>
                              <div class="social-link">
                                   <a href="#" target="_blank"><i class="bx bxl-facebook"></i></a>
                                   <a href="#" target="_blank"><i class="bx bxl-twitter"></i></a>
                                   <a href="#" target="_blank"><i class="bx bxl-linkedin"></i></a>
                                   <a href="#" target="_blank"><i class="bx bxl-instagram"></i></a>
                              </div>
                         </div>
                    </div>
               </div>
               <div class="col-lg-3 col-sm-6 col-md-6">
                    <div class="item-single mb-30">
                         <div class="image">
                              <img src="assets/img/team/team3.jpg" alt="Demo Image">
                         </div>
                         <div class="content">
                              <div class="title">
                                   <h3>
                                        <a href="team.html">Envy Jeqlin</a>
                                   </h3>
                                   <span>Tour Guide</span>
                              </div>
                              <div class="social-link">
                                   <a href="#" target="_blank"><i class="bx bxl-facebook"></i></a>
                                   <a href="#" target="_blank"><i class="bx bxl-twitter"></i></a>
                                   <a href="#" target="_blank"><i class="bx bxl-linkedin"></i></a>
                                   <a href="#" target="_blank"><i class="bx bxl-instagram"></i></a>
                              </div>
                         </div>
                    </div>
               </div>
               <div class="col-lg-3 col-sm-6 col-md-6">
                    <div class="item-single mb-30">
                         <div class="image">
                              <img src="assets/img/team/team4.jpg" alt="Demo Image">
                         </div>
                         <div class="content">
                              <div class="title">
                                   <h3>
                                        <a href="team.html">Brokel Nilson</a>
                                   </h3>
                                   <span>Company Director</span>
                              </div>
                              <div class="social-link">
                                   <a href="#" target="_blank"><i class="bx bxl-facebook"></i></a>
                                   <a href="#" target="_blank"><i class="bx bxl-twitter"></i></a>
                                   <a href="#" target="_blank"><i class="bx bxl-linkedin"></i></a>
                                   <a href="#" target="_blank"><i class="bx bxl-instagram"></i></a>
                              </div>
                         </div>
                    </div>
               </div>
               <div class="col-lg-3 col-sm-6 col-md-6">
                    <div class="item-single mb-30">
                         <div class="image">
                              <img src="assets/img/team/team5.jpg" alt="Demo Image">
                         </div>
                         <div class="content">
                              <div class="title">
                                   <h3>
                                        <a href="team.html">Paul Molive</a>
                                   </h3>
                                   <span>Tour Guide</span>
                              </div>
                              <div class="social-link">
                                   <a href="#" target="_blank"><i class="bx bxl-facebook"></i></a>
                                   <a href="#" target="_blank"><i class="bx bxl-twitter"></i></a>
                                   <a href="#" target="_blank"><i class="bx bxl-linkedin"></i></a>
                                   <a href="#" target="_blank"><i class="bx bxl-instagram"></i></a>
                              </div>
                         </div>
                    </div>
               </div>
               <div class="col-lg-3 col-sm-6 col-md-6">
                    <div class="item-single mb-30">
                         <div class="image">
                              <img src="assets/img/team/team6.jpg" alt="Demo Image">
                         </div>
                         <div class="content">
                              <div class="title">
                                   <h3>
                                        <a href="team.html">Ira Membrit</a>
                                   </h3>
                                   <span>Travel Guide</span>
                              </div>
                              <div class="social-link">
                                   <a href="#" target="_blank"><i class="bx bxl-facebook"></i></a>
                                   <a href="#" target="_blank"><i class="bx bxl-twitter"></i></a>
                                   <a href="#" target="_blank"><i class="bx bxl-linkedin"></i></a>
                                   <a href="#" target="_blank"><i class="bx bxl-instagram"></i></a>
                              </div>
                         </div>
                    </div>
               </div>
               <div class="col-lg-3 col-sm-6 col-md-6">
                    <div class="item-single mb-30">
                         <div class="image">
                              <img src="assets/img/team/team7.jpg" alt="Demo Image">
                         </div>
                         <div class="content">
                              <div class="title">
                                   <h3>
                                        <a href="team.html">Anna Sthesia</a>
                                   </h3>
                                   <span>Assistant Manager</span>
                              </div>
                              <div class="social-link">
                                   <a href="#" target="_blank"><i class="bx bxl-facebook"></i></a>
                                   <a href="#" target="_blank"><i class="bx bxl-twitter"></i></a>
                                   <a href="#" target="_blank"><i class="bx bxl-linkedin"></i></a>
                                   <a href="#" target="_blank"><i class="bx bxl-instagram"></i></a>
                              </div>
                         </div>
                    </div>
               </div>
               <div class="col-lg-3 col-sm-6 col-md-6">
                    <div class="item-single mb-30">
                         <div class="image">
                              <img src="assets/img/team/team8.jpg" alt="Demo Image">
                         </div>
                         <div class="content">
                              <div class="title">
                                   <h3>
                                        <a href="team.html">Brock Lee</a>
                                   </h3>
                                   <span>Managing Director</span>
                              </div>
                              <div class="social-link">
                                   <a href="#" target="_blank"><i class="bx bxl-facebook"></i></a>
                                   <a href="#" target="_blank"><i class="bx bxl-twitter"></i></a>
                                   <a href="#" target="_blank"><i class="bx bxl-linkedin"></i></a>
                                   <a href="#" target="_blank"><i class="bx bxl-instagram"></i></a>
                              </div>
                         </div>
                    </div>
               </div>
               <div class="col-lg-12 col-md-12">
                    <div class="pagination text-center">
                         <span class="page-numbers current" aria-current="page">1</span>
                         <a href="#" class="page-numbers">2</a>
                         <a href="#" class="page-numbers">3</a>
                         <a href="#" class="page-numbers">Next</a>
                    </div>
               </div>
          </div>
     </div>
</section>