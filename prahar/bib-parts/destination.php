<section id="destination" class="destination-section pt-100 pb-70 bg-light">
      <div class="container">
        <div class="section-title">
          <h2>Destinations</h2>
          <p>
            Travel has helped us to understand the meaning of life and it has
            helped us become better people. Each time we travel, we see the
            world with new eyes.
          </p>
        </div>
        <div class="row">
          <div class="col-md-8 m-auto">
            <div class="filter-group">
              <ul id="control" class="list-control">
                <li class="active" data-filter="all">All</li>
                <li data-filter="1">Budget-Friendly</li>
                <li data-filter="2">Royal</li>
                <li data-filter="3">Recommended</li>
              </ul>
            </div>
          </div>
        </div>
        <div class="row filtr-container">
          <div
            class="col-lg-4 col-md-6 filtr-item"
            data-category="1"
            data-sort="value"
          >
            <div class="item-single mb-30">
              <div class="image">
                <img src="assets/img/annapurna.jpg" alt="Annapurna Image" />
              </div>
              <div class="content">
                <span class="location"
                  ><i class="bx bx-map"></i>Pokhara, Nepal</span
                >
                <h3>
                  <a href="destination-details.html">Annapurna Base Camp</a>
                </h3>
                <div class="review">
                  <i class="bx bx-smile"></i>
                  <span>8.5</span>
                  <span>Superb</span>
                </div>
                <p>
                  A wonderful little cottage right on the seashore - perfect for
                  exploring.
                </p>
                <hr />
                <ul class="list">
                  <li><i class="bx bx-time"></i>3 Days</li>
                  <li><i class="bx bx-group"></i>160+</li>
                  <li>$500</li>
                </ul>
              </div>
              <div class="spacer"></div>
            </div>
          </div>
          <div
            class="col-lg-4 col-md-6 filtr-item"
            data-category="2, 1"
            data-sort="value"
          >
            <div class="item-single mb-30">
              <div class="image">
                <img src="assets/img/destination2.jpg" alt="Demo Image" />
              </div>
              <div class="content">
                <span class="location"
                  ><i class="bx bx-map"></i>Jumla, Nepal</span
                >
                <h3>
                  <a href="destination-details.html">Rara</a>
                </h3>
                <div class="review">
                  <i class="bx bx-smile"></i>
                  <span>9</span>
                  <span>Superb</span>
                </div>
                <p>
                  A wonderful little cottage right on the seashore - perfect for
                  exploring.
                </p>
                <hr />
                <ul class="list">
                  <li><i class="bx bx-time"></i>7 Days</li>
                  <li><i class="bx bx-group"></i>65+</li>
                  <li>$2000</li>
                </ul>
              </div>
              <div class="spacer"></div>
            </div>
          </div>
          <div
            class="col-lg-4 col-md-6 filtr-item"
            data-category="2"
            data-sort="value"
          >
            <div class="item-single mb-30">
              <div class="image">
                <img src="assets/img/everest.jpg" alt="Everest Image" />
              </div>
              <div class="content">
                <span class="location"
                  ><i class="bx bx-map"></i>Solukhumbu, Nepal</span
                >
                <h3>
                  <a href="destination-details.html">Everest Base Camp</a>
                </h3>
                <div class="review">
                  <i class="bx bx-smile"></i>
                  <span>7.5</span>
                  <span>Superb</span>
                </div>
                <p>
                  A wonderful little cottage right on the seashore - perfect for
                  exploring.
                </p>
                <hr />
                <ul class="list">
                  <li><i class="bx bx-time"></i>5 Days</li>
                  <li><i class="bx bx-group"></i>96+</li>
                  <li>$2100</li>
                </ul>
              </div>
              <div class="spacer"></div>
            </div>
          </div>
          <div
            class="col-lg-4 col-md-6 filtr-item"
            data-category="2, 3"
            data-sort="value"
          >
            <div class="item-single mb-30">
              <div class="image">
                <img src="assets/img/chitwan.jpg" alt="Demo Image" />
              </div>
              <div class="content">
                <span class="location"
                  ><i class="bx bx-map"></i>Chitwan, Nepal</span
                >
                <h3>
                  <a href="destination-details.html">Chitwan National Park</a>
                </h3>
                <div class="review">
                  <i class="bx bx-smile"></i>
                  <span>9</span>
                  <span>Superb</span>
                </div>
                <p>
                  A wonderful little cottage right on the seashore - perfect for
                  exploring.
                </p>
                <hr />
                <ul class="list">
                  <li><i class="bx bx-time"></i>7 Days</li>
                  <li><i class="bx bx-group"></i>65+</li>
                  <li>$2000</li>
                </ul>
              </div>
              <div class="spacer"></div>
            </div>
          </div>
          <div
            class="col-lg-4 col-md-6 filtr-item"
            data-category="1, 3"
            data-sort="value"
          >
            <div class="item-single mb-30">
              <div class="image">
                <img src="assets/img/destination5.jpg" alt="Demo Image" />
              </div>
              <div class="content">
                <span class="location"
                  ><i class="bx bx-map"></i>Lumbini, Nepal</span
                >
                <h3>
                  <a href="destination-details.html">Lumbini</a>
                </h3>
                <div class="review">
                  <i class="bx bx-smile"></i>
                  <span>8.5</span>
                  <span>Superb</span>
                </div>
                <p>
                  A wonderful little cottage right on the seashore - perfect for
                  exploring.
                </p>
                <hr />
                <ul class="list">
                  <li><i class="bx bx-time"></i>3 Days</li>
                  <li><i class="bx bx-group"></i>160+</li>
                  <li>$1500</li>
                </ul>
              </div>
              <div class="spacer"></div>
            </div>
          </div>
          <div
            class="col-lg-4 col-md-6 filtr-item"
            data-category="3, 1"
            data-sort="value"
          >
            <div class="item-single mb-30">
              <div class="image">
                <img src="assets/img/gosainkunda.jpg" alt="Demo Image" />
              </div>
              <div class="content">
                <span class="location"
                  ><i class="bx bx-map"></i>Rasuwa, Nepal</span
                >
                <h3>
                  <a href="destination-details.html">Gosainkunda</a>
                </h3>
                <div class="review">
                  <i class="bx bx-smile"></i>
                  <span>8.5</span>
                  <span>Superb</span>
                </div>
                <p>
                  A wonderful little cottage right on the seashore - perfect for
                  exploring.
                </p>
                <hr />
                <ul class="list">
                  <li><i class="bx bx-time"></i>3 Days</li>
                  <li><i class="bx bx-group"></i>160+</li>
                  <li>$1500</li>
                </ul>
              </div>
              <div class="spacer"></div>
            </div>
          </div>
        </div>
      </div>
    </section>