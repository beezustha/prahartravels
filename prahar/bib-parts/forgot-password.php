<div class="authentication-section">
     <div class="container">
          <div class="main-form ptb-100">
               <form id="#authForm">
                    <div class="content">
                         <h3>Forgot Your Password?</h3>
                         <p>Enter the email associated with your account and we'll send an email with instruction to
                              reset your password</p>
                    </div>
                    <div class="form-group">
                         <div class="input-icon"><i class="bx bx-at"></i></div>
                         <input type="text" class="form-control" placeholder="Email Address" required>
                    </div>
                    <button type="submit" class="btn-primary">
                         Send
                    </button>
               </form>
          </div>
     </div>
</div>