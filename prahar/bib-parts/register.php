<div class="authentication-section">
     <div class="container">
          <div class="main-form ptb-100">
               <form id="#authForm">
                    <div class="content">
                         <h3>Create Account</h3>
                    </div>
                    <div class="form-group">
                         <div class="input-icon"><i class="bx bx-user"></i></div>
                         <input type="text" class="form-control" placeholder="Username" required>
                    </div>
                    <div class="form-group">
                         <div class="input-icon"><i class="bx bx-at"></i></div>
                         <input type="text" class="form-control" placeholder="Email Address" required>
                    </div>
                    <div class="form-group">
                         <div class="input-icon"><i class="bx bx-show"></i></div>
                         <input type="password" class="form-control" placeholder="Password" required>
                    </div>
                    <div class="row align-items-start mb-30">
                         <div class="col-lg-12">
                              <div class="checkbox">
                                   <input type="checkbox" id="agreement">
                                   <label for="agreement">I agreed Jaunt <a href="terms-of-service.php">Terms of
                                             Services</a> and <a href="privacy-policy.php">Privacy Policy</a></label>
                              </div>
                         </div>
                    </div>
                    <button type="submit" class="btn-primary mb-20">
                         Register
                    </button>
                    <p>Already have an account? <a href="login.php">Login</a></span></p>
               </form>
          </div>
     </div>
</div>