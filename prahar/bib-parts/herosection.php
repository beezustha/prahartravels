<div id="video" class="video-section">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-12">
                <div class="video-content">
                    <a href="https://www.youtube.com/watch?v=i9E_Blai8vk&amp;ab_channel=PriscillaLee"
                        class="youtube-popup video-btn">
                        <i class="bx bx-right-arrow"></i>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>