<!DOCTYPE html>
<html lang="en">

<head>
   <meta charset="UTF-8" />
   <meta name="viewport" content="width=device-width, initial-scale=1.0" />
   <meta name="description" content="Author: HiBootstrap, Category: Tourism, Multipurpose, HTML, SASS, Bootstrap" />

   <title>Jaunt - Travel & Tour Booking HTML Template</title>

   <link rel="stylesheet" href="assets/css/bootstrap.min.css" />

   <link rel="stylesheet" href="assets/css/fontawesome.min.css" />

   <link rel="stylesheet" href="assets/css/boxicons.min.css">

   <link rel="stylesheet" href="assets/css/animate.min.css" />

   <link rel="stylesheet" href="assets/css/bootstrap-datepicker.min.css">

   <link rel="stylesheet" href="assets/css/nice-select.css">

   <link rel="stylesheet" href="assets/css/magnific-popup.min.css" />

   <link rel="stylesheet" href="assets/css/owl.carousel.min.css" />

   <link rel="stylesheet" href="assets/css/meanmenu.min.css" />

   <link rel="stylesheet" href="assets/css/style.css" />

   <link rel="stylesheet" href="assets/css/responsive.css" />

   <link rel="stylesheet" href="assets/css/theme-dark.css" />

   <link rel="icon" href="assets/img/favicon.png" type="image/png" />
</head>

<body>

   <div id="loading">
      <div class="loader"></div>
   </div>


   <header class="header-area">

      <div class="top-header-area">
         <div class="container">
            <div class="row align-items-center">
               <div class="col-xl-6 col-lg-7">
                  <div class="contact-info">
                     <div class="content">
                        <i class="bx bx-phone"></i>
                        <a href="tel:+0123456987">+0123 456 987</a>
                     </div>
                     <div class="content">
                        <i class="bx bx-envelope"></i>
                        <a
                           href="https://templates.hibootstrap.com/cdn-cgi/l/email-protection#6f070a0303002f050e1a011b410c0002"><span
                              class="__cf_email__"
                              data-cfemail="e28a878e8e8da28883978c96cc818d8f">info@prahartravels.com</span></a>
                     </div>
                     <div class="content">
                        <i class="bx bx-map"></i>
                        <a href="#">Mon-Fri: 8 AM – 7 PM</a>
                     </div>
                  </div>
               </div>
               <div class="col-xl-6 col-lg-5">
                  <div class="side-option">

                     <div class="item">
                        <a href="login.php" class="btn-secondary">
                           Login <i class="bx bx-log-in-circle"></i>
                        </a>
                     </div>
                     <div class="item">
                        <a href="#searchBox" id="searchButton" class="btn-search" data-effect="mfp-zoom-in">
                           <i class="bx bx-search-alt"></i>
                        </a>
                        <div id="searchBox" class="search-box mfp-with-anim mfp-hide">
                           <form class="search-form">
                              <input class="search-input" name="search" placeholder="Search" type="text">
                              <button class="btn-search">
                                 <i class="bx bx-search-alt"></i>
                              </button>
                           </form>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="main-navbar-area">
         <div class="main-responsive-nav">
            <div class="container">
               <div class="main-responsive-menu">
                  <div class="logo">
                     <a href="index.php">
                        <img src="assets/img/praharlogo.png" class="logo1" alt="Logo">
                        <img src="assets/img/logo2.png" class="logo2" alt="Logo">
                     </a>
                  </div>
                  <div class="cart responsive">
                     <a href="cart.html" class="cart-btn"><i class="bx bx-cart"></i>
                        <span class="badge">0</span>
                     </a>
                  </div>
               </div>
            </div>
         </div>
         <div class="main-nav">
            <div class="container">
               <nav class="navbar navbar-expand-md navbar-light">
                  <a class="navbar-brand" href="index.php">
                     <img src="assets/img/praharlogo.png" class="logo1" alt="Logo">
                     <img src="assets/img/logo2.png" class="logo2" alt="Logo">
                  </a>
                  <div class="collapse navbar-collapse mean-menu">
                     <ul class="navbar-nav ms-auto">
                        <li class="nav-item">
                           <a href="#" class="nav-link active toggle">Home</a>

                        </li>
                        <li class="nav-item">
                           <a href="#" class="nav-link toggle">Destinations<i class="bx bxs-chevron-down"></i></a>
                           <ul class="dropdown-menu">
                              <li class="nav-item">
                                 <a href="destinations.php" class="nav-link">Destinations</a>
                              </li>
                              <li class="nav-item">
                                 <a href="destination-details.php" class="nav-link">Destinations Details</a>
                              </li>
                           </ul>
                        </li>
                        <li class="nav-item"><a href="#" class="nav-link toggle">Tours<i
                                 class="bx bxs-chevron-down"></i></a>
                           <ul class="dropdown-menu">
                              <li class="nav-item">
                                 <a href="tours.php" class="nav-link">Tours</a>
                              </li>
                              <li class="nav-item">
                                 <a href="offers.php" class="nav-link">Trip Offers</a>
                              </li>
                           </ul>
                        </li>
                        <li class="nav-item"><a href="blogs.php" class="nav-link toggle">Blog<i class="bx"></i></a>

                        </li>
                        <li class="nav-item">
                           <a href="about-us.php" class="nav-link">About Us</a>
                        </li>
                     </ul>
                     <!-- <div class="cart">
                                <a href="cart.html" class="cart-btn"><i class="bx bx-cart"></i>
                                    <span class="badge">0</span>
                                </a>
                            </div> -->
                  </div>
               </nav>
            </div>
         </div>
      </div>

   </header>